var countyData = {
    'type': 'FeatureCollection',
    'features': [{
        'type': 'Feature',
        'properties': {
            'county': 'Maricopa',
            'city': 'Phoenix',
            'state': 'Arizona',
            'country': 'United States Of America',
            'co2Mean': '1.145833',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-112.21743951833471, 33.360580999999996]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'San Francisco',
            'city': 'San Francisco',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.43913',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-122.4199061, 37.7790262]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Solano',
            'city': 'Vallejo',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.326087',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-122.23128894695925, 38.1041081]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Adams',
            'city': 'Welby',
            'state': 'Colorado',
            'country': 'United States Of America',
            'co2Mean': '0.670833',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-104.948394, 39.851353]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Orange',
            'city': 'Winter Park',
            'state': 'Florida',
            'country': 'United States Of America',
            'co2Mean': '0.966667',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-81.37843466574807, 28.5952293]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Cook',
            'city': 'Chicago',
            'state': 'Illinois',
            'country': 'United States Of America',
            'co2Mean': '0.677273',
            'date': '06/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-87.63137574239619, 41.88380505000001]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Boyd',
            'city': 'Ashland',
            'state': 'Kentucky',
            'country': 'United States Of America',
            'co2Mean': '0.36087',
            'date': '29/02/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-82.610449, 38.445711]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Henderson',
            'city': 'Henderson',
            'state': 'Kentucky',
            'country': 'United States Of America',
            'co2Mean': '0.080952',
            'date': '29/02/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-87.5907631, 37.8367513]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Wayne',
            'city': 'Detroit',
            'state': 'Michigan',
            'country': 'United States Of America',
            'co2Mean': '0.9625',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-83.05615022037962, 42.3533947]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Saint Louis',
            'city': 'Ferguson',
            'state': 'Missouri',
            'country': 'United States Of America',
            'co2Mean': '0.954167',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-90.298701, 38.7394188]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Camden',
            'city': 'Camden',
            'state': 'New Jersey',
            'country': 'United States Of America',
            'co2Mean': '1.145833',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.1198911, 39.9448402]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Bronx',
            'city': 'New York',
            'state': 'New York',
            'country': 'United States Of America',
            'co2Mean': '0.866667',
            'date': '05/05/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-73.8785937, 40.8466508]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Queens',
            'city': 'New York',
            'state': 'New York',
            'country': 'United States Of America',
            'co2Mean': '0.775',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-73.7976337, 40.7498243]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Suffolk',
            'city': 'Holtsville',
            'state': 'New York',
            'country': 'United States Of America',
            'co2Mean': '0.75',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-73.05080850726651, 40.80570005]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Forsyth',
            'city': 'Winston-Salem',
            'state': 'North Carolina',
            'country': 'United States Of America',
            'co2Mean': '0.443478',
            'date': '31/08/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.2708838, 36.0690263]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Mecklenburg',
            'city': 'Charlotte',
            'state': 'North Carolina',
            'country': 'United States Of America',
            'co2Mean': '0.75',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.82523928145449, 35.20295565]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Allegheny',
            'city': 'Pittsburgh',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.0181907, 40.4478207]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Beaver',
            'city': 'Beaver Falls',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.045833',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.3116357, 40.7578505]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Berks',
            'city': 'Reading',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.554167',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.94460769660756, 40.3242003]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Blair',
            'city': 'Altoona',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.0125',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-78.3728498, 40.5166626]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Bucks',
            'city': 'Bristol',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.516667',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-74.85398060303687, 40.1563031]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Cambria',
            'city': 'Johnstown',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.5125',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-78.9300787, 40.3402098]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Lackawanna',
            'city': 'Scranton',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.4125',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.65867565883656, 41.41176365]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Lancaster',
            'city': 'Lancaster',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.2125',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-76.3056686, 40.03813]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Lawrence',
            'city': 'New Castle',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.420833',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.37882, 40.960277]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Montgomery',
            'city': 'Norristown',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.069565',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.3338, 40.124]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Northampton',
            'city': 'Freemansburg',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.208333',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.25891820058081, 40.66787525]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Philadelphia',
            'city': 'Philadelphia',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '1.116667',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.1635262, 39.9527237]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Washington',
            'city': 'Charleroi',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.383333',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-79.8981035, 40.1378499]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Westmoreland',
            'city': 'Greensburg',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.408333',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-79.54459184290891, 40.3026858]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'York',
            'city': 'York',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.366667',
            'date': '31/03/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-76.727392, 39.962398]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Dallas',
            'city': 'Dallas',
            'state': 'Texas',
            'country': 'United States Of America',
            'co2Mean': '0.633333',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-96.7968559, 32.7762719]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'El Paso',
            'city': 'El Paso',
            'state': 'Texas',
            'country': 'United States Of America',
            'co2Mean': '0',
            'date': '06/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-106.4646348, 31.7754152]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Harris',
            'city': 'Houston',
            'state': 'Texas',
            'country': 'United States Of America',
            'co2Mean': '1.430435',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-95.3676974, 29.7589382]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Fairfax',
            'city': 'McLean',
            'state': 'Virginia',
            'country': 'United States Of America',
            'co2Mean': '2.258333',
            'date': '01/01/2000'
        },
        'geometry': {'type': 'Point', 'coordinates': [-77.17762038947376, 38.93177245]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Alameda',
            'city': 'Oakland',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.972727',
            'date': '05/12/2001'
        },
        'geometry': {'type': 'Point', 'coordinates': [-122.19937132239198, 37.751675000000006]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Los Angeles',
            'city': 'Los Angeles',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.733333',
            'date': '03/03/2001'
        },
        'geometry': {'type': 'Point', 'coordinates': [-118.2427666, 34.0536909]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Suffolk',
            'city': 'Boston',
            'state': 'Massachusetts',
            'country': 'United States Of America',
            'co2Mean': '0.081818',
            'date': '01/04/2001'
        },
        'geometry': {'type': 'Point', 'coordinates': [-71.06105776439689, 42.356701099999995]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Kent',
            'city': 'Grand Rapids',
            'state': 'Michigan',
            'country': 'United States Of America',
            'co2Mean': '0.216667',
            'date': '31/03/2002'
        },
        'geometry': {'type': 'Point', 'coordinates': [-85.6676096, 42.9634482]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Hillsborough',
            'city': 'Manchester',
            'state': 'New Hampshire',
            'country': 'United States Of America',
            'co2Mean': '0.245833',
            'date': '31/03/2002'
        },
        'geometry': {'type': 'Point', 'coordinates': [-71.4611409251107, 42.988745699999996]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Fresno',
            'city': 'Fresno',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '1.913043',
            'date': '01/01/2003'
        },
        'geometry': {'type': 'Point', 'coordinates': [-119.70886126075588, 36.7295295]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Denver',
            'city': 'Denver',
            'state': 'Colorado',
            'country': 'United States Of America',
            'co2Mean': '0.525',
            'date': '29/06/2005'
        },
        'geometry': {'type': 'Point', 'coordinates': [-104.9848623, 39.7392364]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Jefferson',
            'city': 'Kenner',
            'state': 'Louisiana',
            'country': 'United States Of America',
            'co2Mean': '0.604167',
            'date': '14/09/2005'
        },
        'geometry': {'type': 'Point', 'coordinates': [-90.2509108, 30.0421468]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Erie',
            'city': 'Erie',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.3',
            'date': '31/03/2005'
        },
        'geometry': {'type': 'Point', 'coordinates': [-80.0852695, 42.1294712]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Humboldt',
            'city': 'Eureka',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.333333',
            'date': '15/12/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-124.18841559107143, 40.77729435]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'New Haven',
            'city': 'New Haven',
            'state': 'Connecticut',
            'country': 'United States Of America',
            'co2Mean': '0.157143',
            'date': '04/04/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-72.9250518, 41.3082138]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Scott',
            'city': 'Davenport',
            'state': 'Iowa',
            'country': 'United States Of America',
            'co2Mean': '0.316417',
            'date': '01/10/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-90.5797953, 41.5239048]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Baltimore',
            'city': 'Essex',
            'state': 'Maryland',
            'country': 'United States Of America',
            'co2Mean': '0.718182',
            'date': '15/02/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-76.4881703, 39.3186729]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Tulsa',
            'city': 'Tulsa',
            'state': 'Oklahoma',
            'country': 'United States Of America',
            'co2Mean': '0.488889',
            'date': '23/10/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-95.9929113, 36.1556805]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Luzerne',
            'city': 'Wilkes-Barre',
            'state': 'Pennsylvania',
            'country': 'United States Of America',
            'co2Mean': '0.283333',
            'date': '31/03/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-75.87891453792912, 41.2515797]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Fairfax',
            'city': 'Annandale',
            'state': 'Virginia',
            'country': 'United States Of America',
            'co2Mean': '0.320833',
            'date': '01/01/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-77.2233154, 38.8342792]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Milwaukee',
            'city': 'Milwaukee',
            'state': 'Wisconsin',
            'country': 'United States Of America',
            'co2Mean': '0.3',
            'date': '01/01/2006'
        },
        'geometry': {'type': 'Point', 'coordinates': [-87.922497, 43.0349931]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Pulaski',
            'city': 'North Little Rock',
            'state': 'Arkansas',
            'country': 'United States Of America',
            'co2Mean': '0.836364',
            'date': '01/01/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-92.3007067, 34.7828692]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Alameda',
            'city': 'Berkeley',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.165217',
            'date': '08/12/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-122.2744409, 37.8845244]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Fairfield',
            'city': 'Westport',
            'state': 'Connecticut',
            'country': 'United States Of America',
            'co2Mean': '0.255417',
            'date': '31/03/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-73.3094762418356, 41.148579]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Hartford',
            'city': 'East Hartford',
            'state': 'Connecticut',
            'country': 'United States Of America',
            'co2Mean': '0.4125',
            'date': '31/03/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-72.6908547, 41.764582]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Cook',
            'city': 'Northbrook',
            'state': 'Illinois',
            'country': 'United States Of America',
            'co2Mean': '0.378571',
            'date': '18/04/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-87.8823506, 42.1529132]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Multnomah',
            'city': 'Portland',
            'state': 'Oregon',
            'country': 'United States Of America',
            'co2Mean': '0.3',
            'date': '02/05/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-122.7127865, 45.4661508]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'McLennan',
            'city': 'Waco',
            'state': 'Texas',
            'country': 'United States Of America',
            'co2Mean': '0.245833',
            'date': '20/04/2007'
        },
        'geometry': {'type': 'Point', 'coordinates': [-97.1713209335767, 31.59059865]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Bexar',
            'city': 'San Antonio',
            'state': 'Texas',
            'country': 'United States Of America',
            'co2Mean': '0.2125',
            'date': '01/01/2008'
        },
        'geometry': {'type': 'Point', 'coordinates': [-98.5104781, 29.4263987]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Santa Clara',
            'city': 'San Jose',
            'state': 'California',
            'country': 'United States Of America',
            'co2Mean': '0.426667',
            'date': '10/02/2009'
        },
        'geometry': {'type': 'Point', 'coordinates': [-121.8900141, 37.3363493]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Athens',
            'city': 'Athens',
            'state': 'Ohio',
            'country': 'United States Of America',
            'co2Mean': '0.570833',
            'date': '08/04/2009'
        },
        'geometry': {'type': 'Point', 'coordinates': [-82.1012479, 39.3289242]}
    }, {
        'type': 'Feature',
        'properties': {
            'county': 'Pima',
            'city': 'Tucson',
            'state': 'Arizona',
            'country': 'United States Of America',
            'co2Mean': '0.158333',
            'date': '01/10/2010'
        },
        'geometry': {'type': 'Point', 'coordinates': [-110.9972645, 32.2838359]}
    }]
}