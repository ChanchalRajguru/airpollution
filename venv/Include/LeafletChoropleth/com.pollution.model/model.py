from pymongo import MongoClient
import datetime

demoClient = MongoClient
myClient = MongoClient('localhost', 27017)
db = myClient['test-database']
collection = db['test-collection']

post = {"author": "Mike",
        "text": "My first blog post!",
        "tags": ["mongodb", "python", "pymongo"],
        "date": datetime.datetime.utcnow()}
print(post)
posts = db.posts
post_id = posts.insert_one(post).inserted_id
print(post_id)
print(posts)
print(db.list_collection_names())

