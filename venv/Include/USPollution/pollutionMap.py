import pandas as pd
import folium
import os

states = os.path.join('data','us-states.json')
pollution_data = os.path.join('data','Us.csv')
state_data = pd.read_csv(pollution_data)

m = folium.Map(location=[48,-102], zoom_start=4)

map1 = m.choropleth(
    geo_data=states,
    name='NO2',
    data=state_data,
    columns=['State', 'NO2.1st.Max.Value'],
    key_on='feature.properties.name',
    fill_color='Reds',
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name='NO2 %'
)


#folium.LayerControl().add_to(m)

#carbonMap = folium.Map(location=[48,-102], zoom_start=3)
map2 = m.choropleth(
    geo_data=states,
    name='CO2',
    data=state_data,
    columns=['State', 'CO.1st.Max.Value'],
    key_on='feature.properties.name',
    #fill_color='YlGn',
    fill_color='BuGn',
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name='CO2 %'
)
#L.control.layers(map1, map2).addTo(m);
folium.LayerControl(map1, map2).add_to(m)

m.save('Us_Map.html')

#Creating Custom Legend


