data <- read.csv("pollution_us_2000_2016.csv")

#Dropping columns
data$X <- NULL
data$State.Code <- NULL
data$Address <- NULL
data$County.Code <- NULL
data$Site.Num <- NULL
data$County  <- NULL
data$City <- NULL
data$Date.Local <- NULL

# Taking care of missing data
data$NO2.1st.Max.Value = ifelse(is.na(data$NO2.1st.Max.Value),
                  ave(data$NO2.1st.Max.Value, FUN = function(x) mean(x, na.rm = TRUE)),
                  data$NO2.1st.Max.Value)
newdf <- as.data.frame(data)
write.csv(newdf,"D:\\AirPollution_Project\\Workspace\\APSolution\\venv\\Include\\USPollution\\data\\Us.csv", row.names = FALSE)
